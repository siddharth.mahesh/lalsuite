#!/bin/bash

cd lalsuite&&
make -j4; make install &&
./lalsimulation/bin/lalsim-inspiral -a SEOBNRv4P -f 20.00000000000002 -M 23 -m 10 -X 0.01 -Y -0.02 -Z -0.03 -x 0.04 -y -0.05 -z 0.06 > outputv4Ppertindex0.txt &&
#./lalsimulation/bin/lalsim-inspiral -a SEOBNRv4P -f 20 -M 25 -m 25 -X 0.1 -Y 0. -Z 0. -x -0.1 -y 0. -z -0. > outputv4Pindex1.txt &&
#./lalsimulation/bin/lalsim-inspiral -a SEOBNRv4P -f 20 -M 7 -m 5 -X 0.01 -Y -0.5 -Z 0.03 -x -0.04 -y 0.05 -z -0.06 > outputv4Pindex2.txt &&
git add . ; git commit -m "outputs" ; git push origin master &&
cd ../
